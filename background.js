// name	  : background.js
// original author : Simon Descarpentres
// author : Florian Schmitt <mrflos@lilo.org>
// date   : 2017-06
// licence: GPLv3

/* Open a new tab, and load "index.html" into it. */
function openChaton() {
	console.log("injecting chatons home");
	browser.tabs.create({
		"url": "/index.html"
	});
}
/* Add openMyPage() as a listener to clicks on the browser action. */
browser.browserAction.onClicked.addListener(openChaton);
