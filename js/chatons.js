;(async () => {
  'use strict'
  // open modal and copy title and body
  function openModal(title, bodySelector) {
	document.querySelector('#modal-title').innerText = title;
	document.querySelector('#modal-content').innerHTML = document.querySelector('#'+bodySelector).innerHTML;
    document.getElementById('modal').checked = true
    return false
  }

  // add click event on every button that opens a modal (they need to have data-title and data-content attributes)
  var elems = document.getElementsByClassName('card__btn')
  for (var i = 0; i < elems.length; i++) {
    var current = elems[i]
    current.addEventListener(
      'click',
      function(e) {
        openModal(e.target.getAttribute('data-title'), e.target.getAttribute('data-content'))
      },
      false
    )
  }

  // Change the drupal formatted json into an array per program
  function sortByProgram(json) {
    var programs = ['Etherpad', 'KodiMD', 'Jitsi']
    var tabPrograms = []
    programs.forEach(function(program, index) {
      tabPrograms[program] = filterByProperty(json, 'Logiciels', program)
    })

    console.log(tabPrograms)
    return tabPrograms
  }

  // filter array by property;
  function filterByProperty(array, prop, value) {
    var filtered = []
    for (var i = 0; i < array.length; i++) {
      var obj = array[i]

      for (var key in obj) {
        if (typeof (obj[key] == 'object')) {
          var item = obj[key]
          if (item[prop] == value) {
            filtered.push(item)
          }
        }
      }
    }

    return filtered
  }

  // Create the XHR object.
  function createCORSRequest(method, url) {
    var xhr = new XMLHttpRequest()
    if ('withCredentials' in xhr) {
      // XHR for Chrome/Firefox/Opera/Safari.
      xhr.open(method, url, true)
    } else if (typeof XDomainRequest != 'undefined') {
      // XDomainRequest for IE.
      xhr = new XDomainRequest()
      xhr.open(method, url)
    } else {
      // CORS not supported.
      xhr = null
    }
    return xhr
  }

  // Make the actual CORS request.
  function makeCorsRequest(url) {
    var xhr = createCORSRequest('GET', url)
    if (!xhr) {
      console.log('CORS not supported')
      return
    }

    // Response handlers.
    xhr.onload = function() {
      const json = JSON.parse(xhr.responseText)
      sortByProgram(json.nodes)
      //console.log('Response from CORS request to ' + url + ': ', json.nodes);
    }

    xhr.onerror = function() {
      console.log('Woops, there was an error making the request.')
    }

    xhr.send()
  }

  // TODO : check CORS on chatons.org website
  makeCorsRequest('https://chatons.org/fr/paniere/json');
  //makeCorsRequest('sources/services-chatons.json')
})()
